<?php
$host = 'localhost';
$user = 'root';
$password = '9398';
$db = 'forum';

$connection = mysqli_connect($host, $user, $password, $db);
if (!$connection) {
    die("Connection error");
}

$query = "CREATE TABLE users (
  user_id INT UNSIGNED AUTO_INCREMENT NOT NULL,
  email VARCHAR(30) NOT NULL,
  name VARCHAR(30),
  phone VARCHAR(11),
  last_access DATETIME NOT NULL DEFAULT NOW(),
  PRIMARY KEY (email),
  UNIQUE KEY (user_id)
);";

//$result = mysqli_query($connection, $query);
if (!mysqli_query($connection, $query)) {
    echo "Ошибка создания таблицы users" . PHP_EOL;
} else {
    echo "Таблица users создалась" . PHP_EOL;
}

echo "<br />";

$query = "CREATE TABLE blogs (
  blog_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
  title VARCHAR(100) NOT NULL,
  date_modified TIMESTAMP,
  short_description VARCHAR(100),
  description TEXT,
  image VARCHAR(100),
  user_id INT UNSIGNED NOT NULL,
  PRIMARY KEY (blog_id),
  FOREIGN KEY (user_id) REFERENCES users (user_id)
);";

//$result = mysqli_query($connection, $query);
if (!mysqli_query($connection, $query)) {
    echo "Ошибка создания таблицы blogs" . PHP_EOL;
} else {
    echo "Таблица blogs создалась" . PHP_EOL;
}

?>