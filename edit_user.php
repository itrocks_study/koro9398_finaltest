<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Пользователи</title>
</head>
<body>
<form method="post" action="edit_user.php" style="margin-left: 50%;">
    <input name="email" type="text" placeholder="email">
    <br>
    <input type="text" name="name" placeholder="ФИО">
    <br>
    <input type="text" name="phone" placeholder="Телефон">
    <br>
    <input type="hidden" name="check-add">
    <input type="Submit" value="Внести данные в бд">
</form>
<hr>


<?php
$host = 'localhost';
$user = 'root';
$password = '9398';
$db = 'forum';

$connection = mysqli_connect($host, $user, $password, $db);
if (!$connection) {
    die("Connection error");
}

$query = "SELECT * FROM users ORDER BY (last_access) DESC;";
$result = mysqli_query($connection, $query);
if (!$result) {
    echo "Ошибка выполнения запроса";
    exit;
}

if (mysqli_num_rows($result) === 0) {
    echo "В базе данных отсутствуют данные о пользователях<br>";
} else {
    while ($data = mysqli_fetch_assoc($result)) {
        writeForm($data);
    }
}
?>


<?php
//print_r($_POST);
//Обработчик форм
if(isset($_POST['check-add']) || isset($_POST['check-edit'])) {
//    echo "Данные из формы получены<br>";
//    print_r($_POST);echo "<br>";
    $email = $_POST['email'];
    $name = $_POST['name'];
    $phone = $_POST['phone'];


    if(isset($_POST['check-add'])) {
        $query = "SELECT COUNT(*) AS number FROM users WHERE email = '" . $email . "';";
        $result = mysqli_query($connection, $query);
        $number = mysqli_fetch_assoc($result)['number'];

        if ((int)$number === 0) {
            $query = "INSERT INTO users (email, name, phone) VALUES ('" . $email . "', '" . $name . "', '" . $phone . "')";
            if (!mysqli_query($connection, $query)) {
                echo "<div style='color: red; font-weight: bold;'>Ошибка при внесении информации в бд</div>";
            } else {
                echo "<div style='color: green;'>Данные успешно внесены в бд</div>";
            }
        } else {
            echo "<div style='color: red;'>Пользователь с таким email уже существует</div>";
        }
    }

    if (isset($_POST['check-edit'])) {
        $user_id = $_POST['id'];
        $action = $_POST['action'];

        $query = "SELECT user_id, email FROM users WHERE email = '" . $email . "';";
        $result = mysqli_query($connection, $query);
        $data = mysqli_fetch_assoc($result);

        if($action == 'update') {
            if ((int)mysqli_num_rows($result) == 0 || $data['user_id'] == $user_id && $data['email'] == $email) {
                $query = "UPDATE users SET email = '" . $email . "', name = '" . $name . "', phone='" . $phone . "' WHERE user_id = '" . $user_id . "';";
                if (!mysqli_query($connection, $query)) {
                    echo "<div style='color: red; font-weight: bold;'>Ошибка при обновлении информации в бд</div>";
                } else {
                    echo "<div style='color: green;'>Данные успешно обновлены</div>";
                }
            } else {
                echo "<div style='color: red;'>Ошибка! Попытка обновления поля email уже существующим значением</div>";
            }
        }

        if($action == 'delete') {
            if ($data['user_id'] == $user_id && $data['email'] == $email) {
                $query = "DELETE FROM blogs WHERE user_id = '" . $user_id . "';";
                if (!mysqli_query($connection, $query)) {
                    echo "<div style='color: red; font-weight: bold;'>Ошибка при удалении блогов, свзяанных с полльзователем ". $email ."</div>";
                } else {
                    echo "<div style='color: green;'>Блоги свзяанные с полльзователем ". $email ." успешно удалены</div>";
                }


                $query = "DELETE FROM users WHERE user_id = '" . $user_id . "';";
                if (!mysqli_query($connection, $query)) {
                    echo "<div style='color: red; font-weight: bold;'>Ошибка при удалении пользователя ". $email ."</div>";
                } else {
                    echo "<div style='color: green;'>Пользователь ". $email ." успешно удален</div>";
                }
            } else {
                echo "<div style='color: red;'>Ошибка введенных данных пользователя</div>";
            }
        }

        if (!isset($_POST['action'])) {
            echo "<div style='color: orange;'>Вы не выбрали действия</div>";
        }

    }

    header("REFRESH: 3");
}

?>

<?php
function writeForm($data)
{ ?>
    <form method="post" action="edit_user.php">
        <input type="text" name="id" value="<?php echo $data['user_id']; ?>" readonly>
        <input type="text" name="email" value="<?php echo $data['email']; ?>" placeholder="email">
        <input type="text" name="name" value="<?php echo $data['name']; ?>" placeholder="ФИО">
        <input type="text" name="phone" value="<?php echo $data['phone']; ?>" placeholder="Телефон">
        <input type="text" name="last-access" value="<?php echo $data['last_access']; ?>" readonly>
        <input type="radio" name="action" value="update">Обновить
        <input type="radio" name="action" value="delete">Удалить
        <input type="hidden" name="check-edit">
        <input type="Submit" value="Выполнить">
    </form>
    <hr>
<?php
}
?>
</body>
</html>
